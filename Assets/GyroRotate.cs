﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GyroRotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Input.gyro.attitude;
		Debug.Log((string)Input.inputString);
	}

	public void detectPressedKeyOrButton()
	{
		foreach(KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
		{
			if (Input.GetKeyDown(kcode))
				Debug.Log("KeyCode down: " + kcode);
		}
	}
}
